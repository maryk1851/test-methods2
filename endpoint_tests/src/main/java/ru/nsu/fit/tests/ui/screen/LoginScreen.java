package ru.nsu.fit.tests.ui.screen;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

import java.security.SecureRandom;

public class LoginScreen {
    public LoginScreen(Browser browser, By element, String text, String  message) {
        browser.openPage("http://localhost:8080/endpoint");
        browser.waitForElement(By.id("email"));

        browser.getElement(By.id("email")).sendKeys("admin");
        browser.getElement(By.id("password")).sendKeys("setup");

        browser.getElement(By.id("login")).click();
        browser.typeText(element, text, message);
    }

    public CustomersScreen login() {
        return new CustomersScreen();
    }

    public ClickScreen login() {
        // TODO: Please implement this...
    }
}
