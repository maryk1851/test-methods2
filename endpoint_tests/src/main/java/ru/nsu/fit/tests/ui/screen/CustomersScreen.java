package ru.nsu.fit.tests.ui.screen;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

public class CustomersScreen {
    public CustomersScreen(Browser browser, String firstName, String lastName, String email, String password) {
        browser.click(By.id("add_new_customer"));

        browser.typeText((By.id("first_name_id")), firstName,
                "Try to make screen: field 'first_name_id', value " + firstName);
        browser.typeText((By.id("last_name_id")), lastName,
                "Try to make screen: field 'last_name_id', value " + lastName);
        browser.typeText((By.id("email_id")), email,
                "Try to make screen: field 'email_id', value " + email);
        browser.typeText((By.id("password_id")), password,
                "Try to make screen: field 'password_id', value " + "strongpass");

        browser.click(By.id("create_customer_id"));

        browser.getElement(By.id("customer_list_id"));
    }
}
