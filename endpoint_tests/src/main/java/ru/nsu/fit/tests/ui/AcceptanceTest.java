package ru.nsu.fit.tests.ui;

import org.openqa.selenium.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.Log;
import ru.nsu.fit.tests.ui.screen.CustomersScreen;
import ru.nsu.fit.tests.ui.screen.LoginScreen;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.util.UUID;

public class AcceptanceTest {

    private Browser browser = null;

    private String email = UUID.randomUUID() + "email@example.com";

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }

    @Test
    @Title("Create customer")
    @Description("Create customer via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer feature")
    public void createCustomer() {
        Log.info("Try to make screen from homepage");
        browser.openPage("http://localhost:8080/endpoint");
        browser.waitForElement(By.id("email"));

        browser.typeText((By.id("email")), "admin",
                "Try to make screen: field 'email', value 'admin'");

        browser.typeText((By.id("email")), "admin",
                "Try to make screen: field 'email', value 'admin'");
        new LoginScreen(browser, (By.id("password")), "setup",
                "Try to make screen: field 'password', value 'setup'");

        browser.click(By.id("login"));

        // create customer
        new CustomersScreen(browser,"John", "Weak", email, "strongpass");
    }

    @Test(dependsOnMethods = "createCustomer")
    @Title("Check login")
    @Description("Search customer by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void clickTheNextPage() {
        browser.click(By.id("customer_list_id_next"));
    }

    @Test(dependsOnMethods = "clickTheNextPage")
    @Title("Check login")
    @Description("Search customer by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void searchCustomer() {
        browser.typeText(By.cssSelector("#customer_list_id_filter input"), email,
                "Try to search by email: " + email);
        browser.getElement(By.xpath("//tr[contains(., 'John')]"));
    }

    @Test(dependsOnMethods = "searchCustomer", expectedExceptions = {NoSuchElementException.class})
    @Title("Check login")
    @Description("Get customer with such id")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void newCustomer() {
        new CustomersScreen(browser,"Johnn", "Weakk", email, "strongpass");
       }

    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
}
