package ru.nsu.fit.tests;

import org.testng.Reporter;
import ru.nsu.fit.services.log.Logger;

import java.util.Base64;

public final class Log {
    private Log() {
    }

    public static void info(String message) {
        Logger.info(message);
        Reporter.log(message);
    }

    public static void debug(String message) {
        Logger.debug(message);
        Reporter.log(message);
    }

    public static void screenshot (byte[] bytes) {
        Reporter.log("<img src='data:image/png;base64, "+ Base64.getEncoder().encodeToString(bytes) + "'/>");
    }
}
