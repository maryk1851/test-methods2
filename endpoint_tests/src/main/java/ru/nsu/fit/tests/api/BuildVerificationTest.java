package ru.nsu.fit.tests.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.tests.Log;
import ru.nsu.fit.tests.api.model.CustomerData;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class BuildVerificationTest {
    private static final String URL_ENDPOINT = "http://localhost:8080/endpoint/rest";
    private Client client;
    private ObjectMapper objectMapper = new ObjectMapper();
    private UUID id;

    @BeforeClass(description = "Initialize of HTTP client")
    public void initClient() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        client = ClientBuilder.newClient(clientConfig);
    }

    @Test(description = "Create customer via API.")
    public void createCustomer() throws IOException {
        WebTarget webTarget = client.target(URL_ENDPOINT).path("customers");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Log.debug("Try to make POST...");

        CustomerData customerData = new CustomerData();
        customerData.setFirstName("Johnds");
        customerData.setLastName("Weak");
        customerData.setLogin("helloworld123@login.com");
        customerData.setPass("password123");
        customerData.setBalance(0);

        Response response = invocationBuilder.post(Entity.entity(objectMapper.writeValueAsString(customerData),
                MediaType.APPLICATION_JSON));
        String entity = response.readEntity(String.class);
        Log.info("Response: " + entity);

        Map mapCustomer = new ObjectMapper().readValue(entity, Map.class);
        id = UUID.fromString((String) mapCustomer.get("id"));

        Assert.assertEquals(response.getStatus(), HttpStatus.SC_OK);
        Assert.assertNotNull(id);
    }

    @Test(description = "Try to log into the system.", dependsOnMethods = "createCustomer")
    public void login() throws IOException {
        Log.debug("Try to make GET...");
        Response response = client.target(URL_ENDPOINT).path("customers")
                .queryParam("login", "helloworld123@login.com").request().get();
        String entity = response.readEntity(String.class);

        Log.info("Response: " + entity);

        int indexFirst = entity.indexOf("[");
        int indexSecond = entity.indexOf("]");
        String json = entity.substring(indexFirst + 1, indexSecond);

        Map mapCustomer = objectMapper.readValue(json, Map.class);
        Assert.assertEquals(mapCustomer.get("firstName"), "Johnds");
        Assert.assertEquals(mapCustomer.get("lastName"), "Weak");
        Assert.assertEquals(mapCustomer.get("login"), "helloworld123@login.com");
        Assert.assertEquals(mapCustomer.get("pass"), "password123");
        Assert.assertEquals(mapCustomer.get("balance"), 0);
    }

    @Test(description = "Delete customer", dependsOnMethods = "login")
    public void removeCustomer() {
        Log.debug("Try to make DELETE...");
        Response response = client.target(URL_ENDPOINT).path("customers")
                .queryParam("id", id).request().delete();
        Log.info("Response: " + response.readEntity(String.class));
        Assert.assertEquals(response.getStatus(), HttpStatus.SC_OK);
    }

    @Test(description = "Delete customer with unknown id")
    public void removeCustomerWithUnknownId() {
        Log.debug("Try to make DELETE...");
        Response response = client.target(URL_ENDPOINT).path("customers")
                .queryParam("id", UUID.randomUUID()).request().delete();
        String entity = response.readEntity(String.class);
        Log.info("Response: " + entity);
        Assert.assertEquals(response.getStatus(), HttpStatus.SC_BAD_REQUEST);
        Assert.assertTrue(entity.contains("There is no costumer with id"));
    }

    @Test(description = "Delete customer with null id")
    public void removeCustomerWithNullId() {
        Log.debug("Try to make DELETE...");
        Response response = client.target(URL_ENDPOINT).path("customers")
                .request().delete();
        String entity = response.readEntity(String.class);
        Log.info("Response: " + entity);
        Assert.assertEquals(response.getStatus(), HttpStatus.SC_BAD_REQUEST);
        Assert.assertTrue(entity.contains("Costumer ID is null"));
    }
}
