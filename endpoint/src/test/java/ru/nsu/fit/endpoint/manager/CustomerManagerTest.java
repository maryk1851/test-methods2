package ru.nsu.fit.endpoint.manager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;

import java.util.UUID;

import static org.mockito.Mockito.*;

class CustomerManagerTest {
    private IDBService dbService;
    private CustomerManager customerManager;

    private CustomerPojo createCustomerInput;

    @BeforeEach
    void init() {
        // create stubs for the test's class
        dbService = mock(IDBService.class);
        Logger logger = mock(Logger.class);

        // create the test's class
        customerManager = new CustomerManager(dbService, logger);
    }

    @Test
    void testCreateCustomer() {
        // настраиваем mock.
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;

        CustomerPojo createCustomerOutput = new CustomerPojo();
        createCustomerOutput.id = UUID.randomUUID();
        createCustomerOutput.firstName = "John";
        createCustomerOutput.lastName = "Wick";
        createCustomerOutput.login = "john_wick@gmail.com";
        createCustomerOutput.pass = "Baba_Jaga";
        createCustomerOutput.balance = 0;

        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);

        // Вызываем метод, который хотим протестировать
        CustomerPojo customer = customerManager.createCustomer(createCustomerInput);

        // Проверяем результат выполенния метода
        assertEquals(customer.id, createCustomerOutput.id);

        // Проверяем, что метод по созданию Customer был вызван ровно 1 раз с определенными аргументами
        verify(dbService, times(1)).createCustomer(createCustomerInput);

        // Проверяем, что другие методы не вызывались...
        verify(dbService, times(0)).getCustomers();
    }

    // Как не надо писать тест...
    // Используйте expected exception аннотации или expected exception rule...
    @Test
    void testCreateCustomerWithNullArgument_Wrong() {
        try {
            customerManager.createCustomer(null);
        } catch (IllegalArgumentException ex) {
            assertEquals("Argument 'customerData' is null.", ex.getMessage());
        }
    }

    @Test
    void testCreateCustomerWithNullArgument_Right() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(null));
        assertEquals("Argument 'customerData' is null.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithEasyPassword() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qwe";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password is easy.", exception.getMessage());
    }


    @Test
    void testCreateCustomerWithShortLastName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "N";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("Last name is short.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithLongLastName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Natalyismails";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("Last name is long.", exception.getMessage());
    }

    @Test
    void testCreateCustomerLastNameWithSpace() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wi ck";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("Last name contains spaces.", exception.getMessage());
    }

    @Test
    void testCreateCustomerLastNameWithIncorrectSymbolsCounts() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick1";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("Last name contains incorrect symbols", exception.getMessage());
    }

    @Test
     void testCreateCustomerLastNameWithIncorrectSymbolsFirstLowerCase() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("Last name contains incorrect symbols", exception.getMessage());
    }

    @Test
     void testCreateCustomerLastNameWithIncorrectSymbolsNotFirstUpperCase() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "WicK";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("Last name contains incorrect symbols", exception.getMessage());
    }

    @Test
     void testCreateCustomerLastNameWithIncorrectSymbols() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick:";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("Last name contains incorrect symbols", exception.getMessage());
    }

    @Test
     void testCreateCustomerWithShortFirstName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "N";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("First name is short.", exception.getMessage());
    }

    @Test
     void testCreateCustomerWithLongFirstName() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "Natalyismails";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("First name is long.", exception.getMessage());
    }

    @Test
     void testCreateCustomerFirstNameWithSpace() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "Jo hn";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("First name contains spaces.", exception.getMessage());
    }

    @Test
     void testCreateCustomerFirstNameWithIncorrectSymbolsCounts() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "Jo1hn";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("First name contains incorrect symbols", exception.getMessage());
    }

    @Test
     void testCreateCustomerFirstNameWithIncorrectSymbolsFirstLowerCase() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "john";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("First name contains incorrect symbols", exception.getMessage());
    }

    @Test
     void testCreateCustomerFirstNameWithIncorrectSymbolsNotFirstUpperCase() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "JOhn";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("First name contains incorrect symbols", exception.getMessage());
    }

    @Test
     void testCreateCustomerFirstNameWithIncorrectSymbols() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "Jo_hn";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("First name contains incorrect symbols", exception.getMessage());
    }

    @Test
     void testCreateCustomerWithIncorrectEmail() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@.gmail.com";
        createCustomerInput.pass = "1508gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("Incorrect e-mail", exception.getMessage());
    }

    @Test
     void testCreateCustomerWithDuplicateEmail() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;

        when(dbService.isExistCustomerWithLogin(createCustomerInput.login)).thenReturn(true);

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("This login already exist", exception.getMessage());
    }

    @Test
     void testCreateCustomerShortPass() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "15gbi";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("Password's length should be more or equal 6 symbols and less or equal 12 " +
                "symbols.", exception.getMessage());
    }

    @Test
     void testCreateCustomerLongPass() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "15gbih59s2k64";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("Password's length should be more or equal 6 symbols and less or equal 12 symbols.",
                exception.getMessage());
    }

    @Test
     void testCreateCustomerPassContainsLoginLastnameFirstname() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "joHn_wiCk";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("Password contains customer's login or last name of first name", exception.getMessage());
    }

    @Test
     void testUpdateCustomerNullID() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.id = null;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.updateCustomer(createCustomerInput));
        assertEquals("Costumer ID is null", exception.getMessage());
    }

    @Test
     void testUpdateCustomerNoSuchID() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.id = UUID.randomUUID();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";

        when(dbService.updateCustomer(createCustomerInput)).thenReturn(0);

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.updateCustomer(createCustomerInput));
        assertEquals("There is no costumer with id", exception.getMessage());
    }

    @Test
     void testRemoveCustomerNullID() {
        UUID id = null;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.removeCustomer(id));
        assertEquals("Costumer ID is null", exception.getMessage());
    }

    @Test
     void testRemoveCustomerNoSuchID() {
        UUID id = UUID.randomUUID();

        when(dbService.updateCustomer(createCustomerInput)).thenReturn(0);

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.removeCustomer(id));
        assertEquals("There is no costumer with id", exception.getMessage());
    }

    @Test
     void testTopUpBalanceNullId() {
        UUID id = null;
        int balance = 5;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.topUpBalance(id, balance));
        assertEquals("Costumer ID is null", exception.getMessage());
    }

    @Test
     void testTopUpBalanceZeroAmount() {
        UUID id = UUID.randomUUID();
        int amount = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.topUpBalance(id, amount));
        assertEquals("Amount should be more then 0", exception.getMessage());
    }

    @Test
     void testTopUpBalanceNoSuchID() {
        UUID id = UUID.randomUUID();
        int amount = 5;

        when(dbService.topUpCustomerBalance(id, amount)).thenReturn(0);

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.topUpBalance(id, amount));
        assertEquals("There is no costumer with id", exception.getMessage());
    }
}
