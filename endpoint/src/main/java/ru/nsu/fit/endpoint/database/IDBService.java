package ru.nsu.fit.endpoint.database;

import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;

import java.util.List;
import java.util.UUID;

public interface IDBService {
    CustomerPojo createCustomer(CustomerPojo customerData);

    int updateCustomer(CustomerPojo customerData);

    int deleteCustomer(UUID id);

    int topUpCustomerBalance(UUID id, int amount);

    List<CustomerPojo> getCustomers();

    UUID getCustomerIdByLogin(String customerLogin);

    CustomerPojo getCustomerById(UUID customerId);

    boolean isExistCustomerWithLogin(String customerLogin);

    PlanPojo createPlan(PlanPojo plan);
}
